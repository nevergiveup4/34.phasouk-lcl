int rely = 2;


void setup()
{
  pinMode(rely, OUTPUT);
}

void loop()
{
  digitalWrite(rely, 1);
  delay(1000); // Wait for 1000 millisecond(s)
  digitalWrite(rely, 0);
  delay(1000); // Wait for 1000 millisecond(s)
}